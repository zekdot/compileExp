import queue
from grammar import getGras,firstAndFollow
from status import Status,printStatus,writeToFile
if __name__ == '__main__':
    # 用于查看状态是否已有
    statusSet = set()
    # 用于保存所有状态，方便以后打印
    statusList = []
    # 载入语法列表 语法-序号映射 左部-语法映射
    gramList,gramOrder,gramMap = getGras()

    # 计算first和follow集
    firsts,follows = firstAndFollow(gramList,gramMap)

    # 构造起点
    startState = Status({gramList[0]},gramMap)
    statusSet.add(startState);
    statusList.append((startState,0));

    # 一个队列用于探索新状态
    stateQue = queue.Queue()

    # 将起始状态加入队列
    stateQue.put(startState);

    # 状态编号
    statusNum = 0

    # 移进字典
    move = {}
    # 当有新状态需要探索时执行
    while not stateQue.empty():
        # 拿到当前队列顶端的一个状态
        cur = stateQue.get()
        # 准备当前状态的动作
        move[cur] = {}
        # 新加入的状态
        #statusNum += 1
        # 状态要加入

        # 得到该状态可走的方向
        dires = cur.getDire()
        # 
        for dire in dires:
            # 走个新状态
            state = cur.forward(dire,gramMap);
            # 如果状态没有
            if not state in statusSet:
                # 加到队列，状态集合，状态列表里面去
                stateQue.put(state);
                statusSet.add(state);
                statusNum += 1;
                statusList.append((state,statusNum))
                # 走下一个方向
            
            # if dire == 'ood':
                # print('找到了ood，添加一个move[%s][%s]=%s'%(statusNum-1,dire,statusNum))
            # 加入动作
            move[cur][dire] = state

    printStatus(statusList,gramOrder)
    #print(follows)
    writeToFile(gramList,statusList,gramOrder,move,follows)
