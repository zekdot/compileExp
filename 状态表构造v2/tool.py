term = [':=', 'begin', 'call', 'const', 'do', 'end', 'id', 'if', 'mn', 'ood', 'procedure', 're', 'read', 'td', 'then', 'un', 'var', 'while', 'write']
def getTerm(right):
    for t in term:
        if len(t) < 3:
            if t == 're':
                #print(right)
                if len(right) >= 3 and right[2] == 'a':
                    return 'read';
            if right[0] == t[0] and right[1] == t[1]:
                return t
        else:
            if right[0] == t[0] and right[1] == t[1] and right[2] == t[2]:
                return t
    return right[0]

if __name__ == '__main__':
    print(getTerm('<ddd>end'))